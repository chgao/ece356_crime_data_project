SET foreign_key_checks = 0;
drop table if exists CrimeCount;
SET foreign_key_checks = 1;

select '-----------------------------------------------------------------' as '';
select 'Create CrimeCount' as '';

CREATE TABLE CrimeCount(
    LSOACode char(9) NOT NULL,
    majorType enum('Burglary', 'Violence Against the Person', 'Robbery', 'Theft and Handling', 'Criminal Damage', 'Drugs', 'Fraud or Forgery', 'Other Notifiable Offences', 'Sexual Offences'),
    minorType varchar(50),
    year smallint,
    month tinyint,
    count int,
    borough varchar(255),

    PRIMARY KEY (LSOACode, year, month, minorType),
    FOREIGN KEY (LSOACode) REFERENCES LSOALookup(LSOACode),
    index idx_cnt (majorType, count)
);

LOAD DATA INFILE '/var/lib/mysql-files/data/london_crime_by_lsoa_short.csv'
REPLACE INTO TABLE CrimeCount
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(LSOACode, borough, majorType, minorType, count, year, month);