SET foreign_key_checks = 0;
drop table if exists StopAndSearch;
drop table if exists StopAndSearchPerson;
drop table if exists StopAndSearchWithPeople;
SET foreign_key_checks = 1;

select '-----------------------------------------------------------------' as '';
select 'Create StopAndSearchWithPeople' as '';

-- Intermediate table for Stop And Search Data
CREATE TABLE StopAndSearchWithPeople(
    sasID int NOT NULL AUTO_INCREMENT,
    date DATETIME NOT NULL,
    type enum('Person search', 'Person and Vehicle search', 'Vehicle search') NOT NULL,
    partOfPolicingOperation BOOLEAN, 
    policingOperation BOOLEAN,
    longitude BIGINT,
    latitude BIGINT,
    legislation enum('Police and Criminal Evidence Act 1984 (section 1)', 'Misuse of Drugs Act 1971 (section 23)', 'Firearms Act 1968 (section 47)', 'Criminal Justice Act 1988 (section 139B)', 'Criminal Justice and Public Order Act 1994 (section 60)'),
    objectOfSearch enum('Stolen goods', 'Controlled drugs', 'Article for use in theft', 'Offensive weapons', 'Articles for use in criminal damage', 'Fireworks', 'Evidence of offences under the Act', 'Anything to threaten or harm anyone', 'Firearms'),
    outcome enum('Suspect arrested', 'Nothing found - no further action', 'Offender given drugs possession warning', 'Local resolution', 'Offender given penalty notice', 'Suspect summonsed to court', 'Article found - Detailed outcome unavailable', 'Offender cautioned'),
    outcomeLinkedToObject BOOLEAN,
    removalOfClothing BOOLEAN,
    gender enum('Male', 'Female', 'Other'),
    ageRange enum('under 10', '10-17', '18-24', '25-34', 'over 34'),
    selfEthnicity varchar(255),
    policeIdentifiedEthnicity enum('Asian', 'White', 'Black', 'Mixed', 'Other'),

    PRIMARY KEY (sasID),
    FOREIGN KEY (longitude, latitude) REFERENCES Location(longitude, latitude)
);

LOAD DATA INFILE '/var/lib/mysql-files/data/london-stop-and-search.csv'
REPLACE INTO TABLE StopAndSearchWithPeople
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(type, @date, @partOfPolicingOperation, @policingOperation, @latitude, @longitude, @gender, @ageRange, selfEthnicity, @policeIdentifiedEthnicity, @legislation, @objectOfSearch, outcome, @outcomeLinkedToObject, @removalOfClothing)
set 
sasID = NULL,
partOfPolicingOperation = IF(@partOfPolicingOperation = '', NULL, @partOfPolicingOperation = 'True'),
policingOperation = IF(@policingOperation = '', NULL, @policingOperation = 'True'),
legislation = NULLIF(@legislation, ''),
objectOfSearch = NULLIF(@objectOfSearch, ''),
outcomeLinkedToObject = (@outcomeLinkedToObject = 'True'),
removalOfClothing = (@removalOfClothing = 'True'),
date = STR_TO_DATE(@date, '%Y-%m-%dT%T+00:00'),
longitude = IF(@longitude = '', NULL, @longitude * 1000000),
latitude = IF(@latitude = '', NULL, @latitude * 1000000),
gender = NULLIF(@gender, ''),
ageRange = NULLIF(@ageRange, ''),
policeIdentifiedEthnicity = NULLIF(@policeIdentifiedEthnicity, '');

select '-----------------------------------------------------------------' as '';
select 'Create StopAndSearch' as '';

-- Actual Tables used
CREATE TABLE StopAndSearch(
    sasID int NOT NULL AUTO_INCREMENT,
    date DATETIME NOT NULL,
    type enum('Person search', 'Person and Vehicle search', 'Vehicle search') NOT NULL,
    partOfPolicingOperation BOOLEAN, 
    policingOperation BOOLEAN,
    longitude BIGINT,
    latitude BIGINT,
    legislation enum('Police and Criminal Evidence Act 1984 (section 1)', 'Misuse of Drugs Act 1971 (section 23)', 'Firearms Act 1968 (section 47)', 'Criminal Justice Act 1988 (section 139B)', 'Criminal Justice and Public Order Act 1994 (section 60)'),
    objectOfSearch enum('Stolen goods', 'Controlled drugs', 'Article for use in theft', 'Offensive weapons', 'Articles for use in criminal damage', 'Fireworks', 'Evidence of offences under the Act', 'Anything to threaten or harm anyone', 'Firearms'),
    outcome enum('Suspect arrested', 'Nothing found - no further action', 'Offender given drugs possession warning', 'Local resolution', 'Offender given penalty notice', 'Suspect summonsed to court', 'Article found - Detailed outcome unavailable', 'Offender cautioned'),
    outcomeLinkedToObject BOOLEAN,
    removalOfClothing BOOLEAN,

    PRIMARY KEY (sasID),
    FOREIGN KEY (longitude, latitude) REFERENCES Location(longitude, latitude),
    index idx_type_leg_obj_out_date (type, legislation, objectOfSearch, outcome)
);

-- insert into StopAndSearch
INSERT INTO StopAndSearch(sasID, date, type, partOfPolicingOperation, policingOperation, longitude, latitude, legislation, objectOfSearch, outcome, outcomeLinkedToObject, removalOfClothing)
SELECT sasID, date, type, partOfPolicingOperation, policingOperation, longitude, latitude, legislation, objectOfSearch, outcome, outcomeLinkedToObject, removalOfClothing
FROM StopAndSearchWithPeople;

select '-----------------------------------------------------------------' as '';
select 'Create StopAndSearchPerson' as '';
CREATE TABLE StopAndSearchPerson(
    sasID int NOT NULL,
    gender enum('Male', 'Female', 'Other'),
    ageRange enum('under 10', '10-17', '18-24', '25-34', 'over 34'),
    selfEthnicity varchar(255),
    policeIdentifiedEthnicity enum('Asian', 'White', 'Black', 'Mixed', 'Other'),

    PRIMARY KEY (sasID),
    FOREIGN KEY (sasID) REFERENCES StopAndSearch(sasID),
    index idx_peth_gend_age(policeIdentifiedEthnicity, gender, ageRange)
);

-- Insert into StopAndSearchPerson
INSERT INTO StopAndSearchPerson(sasID, gender, ageRange, selfEthnicity, policeIdentifiedEthnicity)
SELECT sasID, gender, ageRange, selfEthnicity, policeIdentifiedEthnicity
FROM StopAndSearchWithPeople;