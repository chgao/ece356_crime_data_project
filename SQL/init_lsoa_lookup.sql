SET foreign_key_checks = 0;
drop table if exists LSOALookup;
SET foreign_key_checks = 1;

select '-----------------------------------------------------------------' as '';
select 'Create LSOALookup' as '';

CREATE TABLE LSOALookup(
    LSOACode char(9) NOT NULL,
    LSOAName varchar(50),
    LADName varchar(30),
    LADCode char(9),
    MSOACode char(9),
    MSOAName varchar(30),
    LEPCode char(9),
    LEPName varchar(30),

    PRIMARY KEY (LSOACode),
    index idx_lad_msoa_lep (LADCode, MSOACode, LEPCode)
);

LOAD DATA INFILE '/var/lib/mysql-files/data/london-location-lookup.csv'
IGNORE INTO TABLE LSOALookup
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(@dummy, LADCode, LADName, LSOACode, LSOAName, MSOACode, MSOAName, LEPCode, LEPName, @dummy, @dummy, @dummy);