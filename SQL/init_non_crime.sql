SET foreign_key_checks = 0;
drop table if exists NonCrime;
SET foreign_key_checks = 1;

select '-----------------------------------------------------------------' as '';
select 'Create NonCrime' as '';

CREATE TABLE NonCrime(
    id char(64) NOT NULL,
    date DATETIME NOT NULL,
    reportedBy enum('City of London Police', 'Metropolitan Police Service'),
    fallsWithin enum('City of London Police', 'Metropolitan Police Service'),
    longitude BIGINT,
    latitude BIGINT,
    type enum('Anti-social behaviour'),
    outcome varchar(255),
    context varchar(255),

    PRIMARY KEY(id),
    FOREIGN KEY (longitude, latitude) REFERENCES Location(longitude, latitude),
    index idx_type(date)
);

SET foreign_key_checks = 0;
INSERT INTO NonCrime(id, date, reportedBy, fallsWithin, longitude, latitude, type, outcome, context)
SELECT crimeID, date, reportedBy, fallsWithin, longitude, latitude, crimeType, outcome, context
FROM Street as NewStreet
WHERE crimeType = 'Anti-social behaviour'
ON DUPLICATE KEY UPDATE outcome=NewStreet.outcome;
SET foreign_key_checks = 1;