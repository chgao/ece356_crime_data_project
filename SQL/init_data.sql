create database IF NOT Exists Crime_DB;
use Crime_DB;

source /var/lib/mysql-files/SQL/init_lsoa_lookup.sql
source /var/lib/mysql-files/SQL/init_location.sql
source /var/lib/mysql-files/SQL/init_street_data.sql
source /var/lib/mysql-files/SQL/init_crime.sql
source /var/lib/mysql-files/SQL/init_non_crime.sql
source /var/lib/mysql-files/SQL/init_crime_count.sql
source /var/lib/mysql-files/SQL/init_stop_and_search.sql
source /var/lib/mysql-files/SQL/init_victim.sql
