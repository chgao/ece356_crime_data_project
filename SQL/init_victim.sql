SET foreign_key_checks = 0;
drop table if exists CrimeVictim;
drop table if exists NonCrimeVictim;
SET foreign_key_checks = 1;

select '-----------------------------------------------------------------' as '';
select 'Create CrimeVictim' as '';

CREATE TABLE CrimeVictim(
    victimID int NOT NULL AUTO_INCREMENT,
    age int,
    gender enum('Male', 'Female', 'Other'),
    ethnicity enum('Asian', 'White', 'Black', 'Mixed', 'Other'),
    crimeID char(64) NOT NULL,

    PRIMARY KEY (victimID),
    FOREIGN KEY (crimeID) REFERENCES Crime(crimeID),
    index idx_crime_ethn_gend_age (ethnicity, gender, age)
);

select '-----------------------------------------------------------------' as '';
select 'Create NonCrimeVictim' as '';

CREATE TABLE NonCrimeVictim(
    victimID int NOT NULL AUTO_INCREMENT,
    age int,
    gender enum('Male', 'Female', 'Other'),
    ethnicity enum('Asian', 'White', 'Black', 'Mixed', 'Other'),
    nonCrimeID char(64),

    PRIMARY KEY (victimID),
    FOREIGN KEY (nonCrimeID) REFERENCES NonCrime(id),
    index idx_noncrime_ethn_gend_age (ethnicity, gender, age)
);

use Crime_DB;

-- Insert some dummy Crime Victims
INSERT INTO CrimeVictim(age, gender, ethnicity, crimeID)
VALUES(12, 'Male', 'White', 'b4adcc899360d595450a35cbe4d7d71d295bafefef98b2e764ff3bb8b90fafd0');

INSERT INTO CrimeVictim(age, gender, ethnicity, crimeID)
VALUES(7, 'Female', 'Black', '64b14e3efdf9e12425e3ac19f5e72b6d19b5656523d91bc6b888654f1df18495');

INSERT INTO CrimeVictim(age, gender, ethnicity, crimeID)
VALUES(18, 'Female', 'Black', 'd9909143deda1db75d8ba35b701f31b268d9273764ad427b0cfa9dc95a4366dc');

-- Insert some dummy NonCrime Victims
INSERT INTO NonCrimeVictim(age, gender, ethnicity, nonCrimeID)
VALUES(12, 'Male', 'White', (SELECT id FROM NonCrime limit 1));

INSERT INTO NonCrimeVictim(age, gender, ethnicity, nonCrimeID)
VALUES(7, 'Female', 'Black', (SELECT id FROM NonCrime limit 1));

INSERT INTO NonCrimeVictim(age, gender, ethnicity, nonCrimeID)
VALUES(18, 'Female', 'Black', (SELECT id FROM NonCrime limit 1));