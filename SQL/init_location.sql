SET foreign_key_checks = 0;
drop table if exists Location;
SET foreign_key_checks = 1;

select '-----------------------------------------------------------------' as '';
select 'Create Location' as '';

CREATE TABLE Location(
    longitude BIGINT NOT NULL,
    latitude BIGINT NOT NULL,
    locationDescription varchar(255),
    LSOACode char(9),

    PRIMARY KEY (longitude, latitude),
    FOREIGN KEY (LSOACode) REFERENCES LSOALookup(LSOACode),
    index idx_lsoacd(LSOACode)
);

LOAD DATA INFILE '/var/lib/mysql-files/data/london-outcomes.csv'
IGNORE INTO TABLE Location
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(@dummy, @dummy, @dummy, @dummy, @longitude, @latitude, locationDescription, @LSOACode, @dummy, @dummy)
set 
longitude = IF(@longitude = '', NULL, @longitude * 1000000),
latitude = IF(@latitude = '', NULL, @latitude * 1000000),
LSOACode = NULLIF(@LSOACode, '');

LOAD DATA INFILE '/var/lib/mysql-files/data/london-street.csv'
IGNORE INTO TABLE Location
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(@dummy, @dummy, @dummy, @dummy, @longitude, @latitude, locationDescription, @LSOACode, @dummy, @dummy, @dummy, @dummy)
set 
longitude = IF(@longitude = '', NULL, @longitude * 1000000),
latitude = IF(@latitude = '', NULL, @latitude * 1000000),
LSOACode = NULLIF(@LSOACode, '');

LOAD DATA INFILE '/var/lib/mysql-files/data/london-stop-and-search.csv'
IGNORE INTO TABLE Location
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(@dummy, @dummy, @dummy, @dummy, @latitude, @longitude, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy)
set 
longitude = IF(@longitude = '', NULL, @longitude * 1000000),
latitude = IF(@latitude = '', NULL, @latitude * 1000000);
