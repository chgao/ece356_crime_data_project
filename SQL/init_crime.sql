SET foreign_key_checks = 0;
drop table if exists Crime;
SET foreign_key_checks = 1;

select '-----------------------------------------------------------------' as '';
select 'Create Crime' as '';

CREATE TABLE Crime(
    crimeID char(64) not null,
    date DATETIME NOT NULL,
    reportedBy enum('City of London Police', 'Metropolitan Police Service'),
    fallsWithin enum('City of London Police', 'Metropolitan Police Service'),
    longitude BIGINT,
    latitude BIGINT,
    crimeType enum('Vehicle crime', 'Violence and sexual offences', 'Bicycle theft', 'Other theft', 'Theft from the person', 'Other crime', 'Drugs', 'Burglary', 'Public order', 'Shoplifting', 'Criminal damage and arson', 'Robbery', 'Possession of weapons'),
    outcome varchar(255),
    context varchar(255),

    PRIMARY KEY(crimeID),
    FOREIGN KEY (longitude, latitude) REFERENCES Location(longitude, latitude),
    index idx_ct_date (crimeType, date)
);

SET foreign_key_checks = 0;
LOAD DATA INFILE '/var/lib/mysql-files/data/london-outcomes.csv'
REPLACE INTO TABLE Crime
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(crimeID, @date, reportedBy, fallsWithin, @longitude, @latitude, @dummy, @dummy, @dummy, outcome)
set 
date = STR_TO_DATE(CONCAT(@date,'-01'), '%Y-%m-%d'),
longitude = IF(@longitude = '', NULL, @longitude * 1000000),
latitude = IF(@latitude = '', NULL, @latitude * 1000000),
context = NULL;
SET foreign_key_checks = 1;

-- Used to reject empty crimeID
INSERT INTO Crime(crimeID, date)
VALUE ('', SYSDATE());

SET foreign_key_checks = 0;
INSERT INTO Crime(crimeID, date, reportedBy, fallsWithin, longitude, latitude, crimeType, outcome, context)
SELECT crimeID, date, reportedBy, fallsWithin, longitude, latitude, crimeType, outcome, context
FROM Street as NewStreet
WHERE crimeType != 'Anti-social behaviour'
ON DUPLICATE KEY UPDATE outcome=NewStreet.outcome;
SET foreign_key_checks = 1;