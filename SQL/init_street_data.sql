-- This is an intermediate table used to create Crime and NonCrime Tables.
SET foreign_key_checks = 0;
drop table if exists Street;
SET foreign_key_checks = 1;

select '-----------------------------------------------------------------' as '';
select 'Create Street' as '';

CREATE TABLE Street(
    crimeID char(64) not null,
    date DATETIME NOT NULL,
    reportedBy enum('City of London Police', 'Metropolitan Police Service'),
    fallsWithin enum('City of London Police', 'Metropolitan Police Service'),
    longitude BIGINT,
    latitude BIGINT,
    locationDescription varchar(255),
    LSOACode char(9),
    LSOAName varchar(50),
    crimeType enum('Vehicle crime', 'Violence and sexual offences', 'Bicycle theft', 'Other theft', 'Theft from the person', 'Other crime', 'Drugs', 'Burglary', 'Public order', 'Shoplifting', 'Criminal damage and arson', 'Robbery', 'Possession of weapons', 'Anti-social behaviour'),
    outcome varchar(255),
    context varchar(255),

    PRIMARY KEY(crimeID),
    FOREIGN KEY (longitude, latitude) REFERENCES Location(longitude, latitude)
);


-- Used to reject empty crimeID
INSERT INTO Street(crimeID, date)
VALUE ('', SYSDATE());

LOAD DATA INFILE '/var/lib/mysql-files/data/london-street.csv'
IGNORE INTO TABLE Street
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(@crimeID, @date, reportedBy, fallsWithin, @longitude, @latitude, locationDescription, @LSOACode, LSOAName, @crimeType, outcome, context)
set 
crimeID = IF(@crimeID = '', UUID(), @crimeID),
date = STR_TO_DATE(CONCAT(@date,'-01'), '%Y-%m-%d'),
longitude = IF(@longitude = '', NULL, @longitude * 1000000),
latitude = IF(@latitude = '', NULL, @latitude * 1000000),
LSOACode = NULLIF(@LSOACode, ''),
crimeType = NULLIF(@crimeType, '');