setup:
	sh ./scripts/setup.sh

connect:
	docker exec -it ece356_db bash -c "mysql -uroot -proot;"
	
run:
	python CLI/main.py

clean:
	sh ./scripts/remove.sh