# ECE356_Crime_Data_Project

## Initialization

**`docker` and `docker-compose` must be installed before the MYSQL server can be initialized.**
- was test on Windows 10 using docker desktop, using Python 3.9.1

1. To start the MYSQL local server, run
- `make setup`

2. To connect to the MYSQL CLI, run
- `make connect`

3. To remove the volumes created by docker compose, run 
- `make clean`
    - this will remove stopped service containers.

4. To run the CLI, run
- `make run`

The ER diagram can be found in `ER.pdf` and the report can be found in `ECE356_Crime_report.pdf`
