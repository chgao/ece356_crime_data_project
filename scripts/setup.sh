python -m pip install -r scripts/requirements.txt

docker-compose up --detach
echo "starting container. Waiting for container to finish starting..."
sleep 20
echo "finished waiting. loading db..."

docker exec -it ece356_db bash -c "cd var/lib/mysql-files/;mysql -uroot -proot < ./SQL/init_data.sql"
echo "finished loading."
docker exec -it ece356_db bash -c "mysql -uroot -proot Crime_DB;"
