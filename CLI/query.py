from tabulate import tabulate
from decimal import *


def construct(dct: dict, arr: list, column: str, like=False) -> str:

    n = 0
    while '' in arr: arr.remove('')

    # d_race = {1: "White", 2: "Black", 3: "Asian", 4: "Other"}
    # c_rage = construct(d_race, race.split(' '), "race" if race != '' else '')

    clause = "("

    for i in arr:

        if like: 
            clause = clause + "{0} LIKE '%{1}%'".format(column, i)
        else: 
            clause = clause + "{0} = '{1}'".format(column, dct[int(i)])
        if n != len(arr) - 1: clause = clause + " OR "
        n += 1
    
    return clause + ")" if n > 0 else ""


def make_query(select, joint_table, args, order, limit, cursor):
    query = f""" SELECT {select} FROM {joint_table} """
    args = list(filter(None, args))
    if args:
        query += " WHERE "
        for i, a in enumerate(args):
            if a == '': continue
            else: query = query + a 
            if i != len(args) - 1: query = query + " AND "

    if order: query += order

    query += f" limit {limit} ;"
    
    print(query)

    cursor.execute(query)
    results = cursor.fetchall()
    header = [i[0] for i in cursor.description]
    # for val in results:
    #     print(val)
    getcontext().prec = 10
    for i, e in enumerate(header):
        if e == 'longitude' or e == 'latitude':

            for y in range(len(results)):
                results[y] = list(results[y])
                for x in range(len(results[0])):
                    if x == i and results[y][x]:
                        results[y][x] = Decimal(results[y][x])
                        results[y][x] /= 1000000

    print(tabulate(results, headers=header, tablefmt='psql', floatfmt=".8f"))


def query_CrimeCount(cursor):
    args = []
    joint_table = "CrimeCount"
    
    lsoa_code = input("Enter the LSOA code (leave empty for all):\n> ")
    c_lsoa = construct({}, lsoa_code.split(', '), "LSOACode", like=True)
    args.append(c_lsoa)

    borough = input("Enter the name of the borough separated by ',' (leave empty for all): \n> ")
    c_borough = construct({}, borough.split(', '), "borough", like=True)
    args.append(c_borough)

    major_category = input("""Select the major crime category: 
	1. Burglary
	2. Criminal Damage
	3. Drugs
	4. Fraud or Forgery
	5. Robbery
	6. Sexual Offenses
	7. Theft and Handling
	8. Violences Against the Person
	9. Other Notifiable Offences\n> """)
    d_major = {
	1: "Burglary",
	2: "Criminal Damage",
	3: "Drugs",
	4: "Fraud or Forgery",
	5: "Robbery",
	6: "Sexual Offenses",
	7: "Theft and Handling",
	8: "Violences Against the Person",
	9: "Other Notifiable Offences"
    }
    c_major = construct(d_major, major_category.split(' '), "majorType")
    args.append(c_major)

    minor_category = input("Enter the minor crime category separated by ',' (leave empty for all): \n> ")
    c_minor = construct({}, minor_category.split(', '), "minorType", like=True)
    args.append(c_minor)
    
    value = input("Enter the range of crime count in the format 'lower bound, upper bound': \n> ")
    v_arr = value.strip().split(',')
    c_value = ""
    while '' in v_arr: v_arr.remove('')
    if len(v_arr) == 2:
        lower_v, upper_v = v_arr[0], v_arr[1]
        c_value = f'(count >= {int(lower_v)} AND count <= {int(upper_v)})'
    args.append(c_value)

    
    start_date = input("Enter the start date in the format yyyy-mm:\n> ")
    end_date = input("Enter the end date in the format yyyy-mm:\n> ")

    if start_date.strip() != '':
        start_date = start_date.split('-')
        start_year = start_date[0]
        start_month = start_date[1]
        
        c_start = f" (year >= {start_year} AND month >= {start_month}) "
        args.append(c_start)

    if end_date.strip() != '':
        end_date = end_date.split('-')
        end_year = end_date[0]
        end_month = end_date[1]

        c_end = f" (year <= {end_year} AND month <= {end_month}) "
        args.append(c_end)


    sort_col = input("""Select the column to sort by the results:
	1. LSOA code
	2. Borough
	3. Major crime category
	4. Minor crime category
	5. Crime count
	6. Date\n> """)
    d_sort_col = {
	1: "LSOACode",
	2: "borough",
	3: "majorType",
	4: "minorType ",
	5: "count",
	6: "date",
    }
    
    c_sort_col = ""
    if sort_col != '':
        sort_by = input("""Select the ordering:
	1. Asending 
	2. Descending\n> """)
        c_sort_col = f' ORDER BY {d_sort_col[int(sort_col)]} ASC ' if sort_by == '1' else f' ORDER BY {d_sort_col[int(sort_col)]} DESC '


    # exec info
    order = c_sort_col

    select = "*"
    count_or_not = input("Do you want the sum of the result? (Y/N - default is N)\n> ")
    if count_or_not.upper() == "Y":
        select = "sum(count)"
        count_or_not = True
    else:
        count_or_not = False

    limit = input("Enter the max rows returned (default is 100):\n> ")
    if limit.strip() == "":
        limit = '100'

    sql_statement = make_query(select, joint_table, args, order, limit, cursor)


def query_crime_non(cursor):

    # outcomes and streets
    # if anti social, no crime id
    args = []

    behaviour = input("Are you looking for entries that are not crimes? (Y/N):\n>  ")
    joint_table = ""
    if behaviour.upper() == "Y": 
        joint_table = "NonCrime" 
        # crime id
        crime_id = input("Enter the ID (or leave empty if all):\n> ")
        c_crime_id = construct({}, crime_id.split(', '), "id", like=True)
        args.append(c_crime_id)

    else: 
        joint_table = "Crime"
        # crime id
        crime_id = input("Enter the crime ID (or leave empty if all):\n> ")
        c_crime_id = construct({}, crime_id.split(', '), "crimeID", like=True)
        args.append(c_crime_id)

    
    # date
    start_date = input("Enter the start date and time in the following format yyyy-mm-dd hh:mm (leave empty for no lower bound):\n> ")
    start_date = start_date.strip()
    if start_date != '':
        c_start_date = f" ( date >= '{start_date}:00' ) "
        args.append(c_start_date)

    end_date = input("Enter the end date and time in the following format yyyy-mm-dd hh:mm (leave empty for no upper bound):\n> ")
    end_date = end_date.strip()
    if end_date != '':
        c_end_date = f" ( date <= '{end_date}:00' ) "
        args.append(c_end_date)


    falls_within = input("""Select the reporter of data (leave empty for all):
	1. Metropolitan Police Service 
	2. City of London Police\n> """)
    d_falls = {1: "Metropolitan Police Service", 2: "City of London Police"}
    tarr = falls_within.split(' ')
    c_falls = construct(d_falls, tarr, "fallsWithin") if not (1 in tarr and 2 in tarr) else ''
    args.append(c_falls)


    reported_by = input("""Select the provider of data (leave empty for all): 
	1. Metropolitan Police Service 
	2. City of London Police\n> """)
    tarr = reported_by.split(' ')
    c_reported = construct(d_falls, tarr, "fallsWithin") if not (1 in tarr and 2 in tarr) else ''
    args.append(c_reported)

    # join Location
    location = input("Enter the street names of crime (leave empty for all):\n> ")
    join_location = False
    if location.strip() != "":
        join_location = True
        c_location = construct({}, location.split(', '), "locationDescription", like=True)

        args.append(c_location)

    # LSOA name
    LSOA_name = input("Enter the LSOA name(s), leave empty if all:\n> ")
    if LSOA_name.strip() != "":
        join_location = True
        c_lsoa_name = construct({}, LSOA_name.split(', '), "LSOAName", like=True)
        args.append(c_lsoa_name)

    # code
    LSOA_code = input("Enter the LSOA codes(s), leave empty if all or 'n' for null:\n> ")
    if LSOA_code.strip() != "":
        join_location = True
        c_lsoa_code = construct({}, LSOA_code.split(', '), "LSOACode", like=True)
        args.append(c_lsoa_code)
    
    if join_location:
        joint_table = joint_table + " inner join Location using (longitude, latitude) "

    # join Victim
    victim = input("Enter the victim description of crime (Y/N or leave empty for all):\n> ")
    if victim.strip() != "" and victim.strip().upper() != "N" :

        victim_age = input("Enter the victim age range 'lower bound, upper bound':\n> ")
        if victim_age.strip() != "":
            v_arr = victim_age.split(',')
            c_value = ""
            while '' in v_arr: v_arr.remove('')
            if len(v_arr) == 2:
                lower_v, upper_v = v_arr[0], v_arr[1]
                c_value = f'(age >= {int(lower_v)} AND age <= {int(upper_v)})'
            args.append(c_value)
        
        victim_gender = input("""Enter the victim gender:
	1. Male
	2. Female
	3. Other\n> """)
        d_victim = {
	1: "Male",
	2: "Female",
	3: "Other"
        }
        c_gender = construct(d_victim, victim_gender.split(' '), "gender")
        args.append(c_gender)

        victim_ethnicity = input("""Enter the victim ethnicity:
	1. Asian
	2. White
	3. Black
	4. Mixed
	5. Other\n> """)
        d_victim = {
	1: "Asian",
	2: "White",
	3: "Black",
	4: "Mixed ",
	5: "Other"
        }
        c_ethnicity = construct(d_victim, victim_ethnicity.split(' '), "ethnicity")
        args.append(c_ethnicity)

        if behaviour.upper() == "Y": 
            joint_table = joint_table + " inner join NonCrimeVictim using (id) "
        else: 
            joint_table = joint_table + " inner join CrimeVictim using (crimeID) "
    

    # only if crime table
    if behaviour.upper() != "Y":
        tempList = ['Vehicle crime', 'Violence and sexual offences', 'Bicycle theft', 'Other theft', 'Theft from the person', 'Other crime', 'Drugs', 'Burglary', 'Public order', 'Shoplifting', 'Criminal damage and arson', 'Robbery', 'Possession of weapons']
        out = ""
        d_crime_type = {}
        for i, ele in enumerate(tempList):
            out += f"\t{i+1}. {ele}\n"
            d_crime_type.update({i+1: ele})
        crime_type = input(f"""Select the type of crime (leave empty if all): \n{out}> """)
        
        c_crime_type = construct(d_crime_type, crime_type.split(' '), "crimeType")
        args.append(c_crime_type)

    # exec info
    select = "*"
    count_or_not = input("Do you want a count of the result? (Y/N - default is N)\n> ")
    if count_or_not.upper() == "Y":
        select = "count(*)"
        count_or_not = True
    else:
        count_or_not = False

    # unique names, locations, count, or entire table
    # default 100, unless they set one

    limit = input("Enter the max rows returned (default is 100):\n> ")
    if limit.strip() == "":
        limit = '100'

    # execution
    sql_statement = make_query(select, joint_table, args, None, limit, cursor)
    

def query_StopAndSearch(cursor):

    args = []
    joint_table = "StopAndSearch"

    search_type = input("""Select type of searching (leave empty for all): 
	1. Person search
	2. Vehicle search
	3. Person and vehicle search\n> """)
    d_search_type = { 1 : 'Person search', 2 : 'Vehicle search', 3 : 'Person and Vehicle search'}
    if search_type.strip() != '':
        c_search_type = f'(type = \'{d_search_type[int(search_type)]}\')' 
        # search type input cannot be null here

        args.append(c_search_type)
    
    # date
    start_date = input("Enter the start date and time in the following format yyyy-mm-dd hh:mm (leave empty for no lower bound):\n> ")
    start_date = start_date.strip()
    if start_date != '':
        c_start_date = f" ( date >= '{start_date}:00' ) "
        args.append(c_start_date)

    end_date = input("Enter the end date and time in the following format yyyy-mm-dd hh:mm (leave empty for no upper bound):\n> ")
    end_date = end_date.strip()
    if end_date != '':
        c_end_date = f" ( date <= '{end_date}:00' ) "
        args.append(c_end_date)


    po = input("Are you looking for entries that are part of a policing operation (Y/N, leave empty for all):\n>  ")
    if po.upper() == "Y": 
        args.append(" (partOfPolicingOperation = True) ")
    elif po.upper() == "N":
        args.append(" (partOfPolicingOperation = False) ")


    tempList = ['Stolen goods', 'Controlled drugs', 'Article for use in theft', 'Offensive weapons', 'Articles for use in criminal damage', 'Fireworks', 'Evidence of offences under the Act', 'Anything to threaten or harm anyone', 'Firearms']
    out = ""
    d_object = {}
    for i, ele in enumerate(tempList):
        out += f"\t{i+1}. {ele}\n"
        d_object.update({i+1: ele})
    object = input(f"""Select the object of search in the stop-and-search (leave empty if all): \n{out}> """)
    
    c_object = construct(d_object, object.split(' '), "objectOfSearch")
    args.append(c_object)

    tempList = ['Suspect arrested', 'Nothing found - no further action', 'Offender given drugs possession warning', 'Local resolution', 'Offender given penalty notice', 'Suspect summonsed to court', 'Article found - Detailed outcome unavailable', 'Offender cautioned']
    out = ""
    d_outcome = {}
    for i, ele in enumerate(tempList):
        out += f"\t{i+1}. {ele}\n"
        d_outcome.update({i+1: ele})
    outcome = input(f"""Select the outcome in the stop-and-search (leave empty if all): \n{out}> """)
    
    c_outcome = construct(d_outcome, outcome.split(' '), "outcome")
    args.append(c_outcome)


    # join Person
    victim = input("Enter the person description of stop-and-search (Y/N or leave empty for all):\n> ")
    if victim.strip() != "" and victim.strip().upper() != "N" :

        age = input(
        """Select the age range of the person (leave empty if all): 
	1. 10-17
	2. 18-24
	3. 25-34
	4. over 34\n> """)
        d_age = {1: "10-17", 2: "18-24", 3: "25-34", 4: "over 34"}
        c_age = construct(d_age, age.split(' '), "ageRange")

        args.append(c_age)
        
        victim_gender = input("""Enter the person's gender:
	1. Male
	2. Female
	3. Other\n> """)
        d_victim = {
	1: "Male",
	2: "Female",
	3: "Other"
        }
        c_gender = construct(d_victim, victim_gender.split(' '), "gender")
        args.append(c_gender)

        victim_ethnicity = input("""Enter the person's ethnicity:
	1. Asian
	2. White
	3. Black
	4. Mixed
	5. Other\n> """)
        d_victim = {
	1: "Asian",
	2: "White",
	3: "Black",
	4: "Mixed ",
	5: "Other"
        }
        c_ethnicity = construct(d_victim, victim_ethnicity.split(' '), "policeIdentifiedEthnicity")
        args.append(c_ethnicity)

        joint_table = joint_table + " inner join StopAndSearchPerson using (sasID) "

    # exec info
    select = "*"
    count_or_not = input("Do you want a count of the result? (Y/N - default is N)\n> ")
    if count_or_not.upper() == "Y":
        select = "count(*)"
        count_or_not = True
    else:
        count_or_not = False

    order = None

    limit = input("Enter the max rows returned (default is 100):\n> ")
    if limit.strip() == "":
        limit = '100'

    sql_statement = make_query(select, joint_table, args, order, limit, cursor)

