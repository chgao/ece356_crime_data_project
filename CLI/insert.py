def make_insertion(table, args, conn):

    cols, values = "", ""

    new_args = {}

    for key in args:
        if type(args[key]) == str and args[key].strip() == '': continue
        else: new_args.update({key: args[key]})
        

    for i, key in enumerate(new_args):
        cols += key
        values += f'\'{new_args[key]}\''

        if i != len(new_args) - 1:
            cols += ", "
            values += ", "

    query = f"""INSERT INTO {table} ({cols}) VALUES ({values});"""

    # print(query)

    cursor = conn.cursor()
    cursor.execute(query)
    conn.commit()

    print(cursor.rowcount, "record inserted.")
    print(f"ID of row inserted: {cursor.lastrowid}")

    return cursor.lastrowid