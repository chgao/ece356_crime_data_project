def make_update(table, args, where, conn):

    if not args:
        print("Nothing to update")

    query = f"UPDATE {table} SET "
    i = 0
    for key, val in args.items():
        query += f" {key}='{val}' "
        if i != len(args)-1:
            query += ","
        i += 1
    
    if where:
        query += " WHERE "
        for i, a in enumerate(where):
            if a == '': continue
            else: query = query + a + f"= '{where[a]}'"
            if i != len(where) - 1: query = query + " AND "

    query += ";"

    # print(query)

    cursor = conn.cursor()
    cursor.execute(query)
    conn.commit()

    print(cursor.rowcount, "record changed.")