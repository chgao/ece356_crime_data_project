import mysql.connector

from query import *
from insert import *
from update import *


def main():

    mydb = mysql.connector.connect(
        host="127.0.0.1",
        user="root",
        password="root",
        port=3307,
        database='Crime_DB'
    )
    cursor = mydb.cursor()
    # cursor = None
    
    while True:

        try:

            user_option = input("""Select an option:
    \t1. Insert crime data
    \t2. Search crime data
    \t3. Update crime data \n> """)

            # INSERT table
            if user_option == '1':

                table = input(
    """Select the table that you wish to insert in (enter nothing to return to the previous menu):
    \t1. Crimes and Non-crimes
    \t2. Stop-and-Search
    \t3. Victim\n> """)
                
                # Crimes and Non-crimes
                if table == '1':

                    args = {}
                    location_args = {}

                    behaviour = input(
                        """Which table are you inserting data into: 
    \t1. Crime
    \t2. Non-crime\n>  """).strip()
                    
                    if behaviour.upper() == '1': joint_table = "Crime"
                    elif behaviour.upper() == '2': joint_table = "NonCrime"
                    else: continue

                    if joint_table == "Crime" :
                        crime_id = ''
                        while crime_id == '': crime_id = input("Enter the crime id (required):\n> ")
                        args.update({"crimeID": crime_id})
                    else:
                        crime_id = ''
                        while crime_id == '': crime_id = input("Enter the id (required):\n> ")
                        args.update({"id": crime_id})

                    date = ''
                    while date == '': date = input("Enter the date of crime in the following format yyyy-mm-dd hh:mm (required):\n> ")
                    args.update({"date": date})
                    
                    falls_within = input("""Select the reporter of data (optional): 
    \t1. Metropolitan Police Service 
    \t2. City of London Police\n> """)
                    d_falls = {1: "Metropolitan Police Service", 2: "City of London Police"}
                    if falls_within != "" :args.update({"fallsWithin": d_falls[int(falls_within)]})

                    reported_by = input("""Select the provider of data (optional): 
        1. Metropolitan Police Service 
        2. City of London Police\n> """)
                    if reported_by != "" : args.update({"reportedBy": d_falls[int(reported_by)]})

                    # join Location
                    location = input("Do you have the location data of crime (Y/N):\n> ")
                    if location.upper() == "Y":
                        long, lat = "", ""
                        while long == "": long = input("Enter the longitude (required):\n> ")
                        while lat == "": lat = input("Enter the latitude (required):\n> ")
                        locDesc = input("Enter the location description (optional, preferrably a street name):\n> ")
                        LSOAcode = input("Enter the LSOA code associated with the location (optional):\n> ")
                        location_args.update({ 
                            'longitude':int(float(long) * 1000000), 
                            'latitude':int(float(lat) * 1000000), 
                            'locationDescription':locDesc, 
                            'LSOAcode':LSOAcode 
                        })
                        args.update({ 'longitude' : int(float(long) * 1000000), 'latitude':int(float(lat) * 1000000) })

                    elif location.upper() == "N":
                        pass

                    # only if crime table
                    if joint_table == "Crime":
                        crime_type = ""
                        
                        tempList = ['Vehicle crime', 'Violence and sexual offences', 'Bicycle theft', 'Other theft', 'Theft from the person', 'Other crime', 'Drugs', 'Burglary', 'Public order', 'Shoplifting', 'Criminal damage and arson', 'Robbery', 'Possession of weapons']
                        out = ""
                        d_crime_type = {}
                        for i, ele in enumerate(tempList):
                            out += f"\t{i+1}. {ele}\n"
                            d_crime_type.update({i+1: ele})
                        while crime_type == "": crime_type = input(f"""Select the type of crime it falls under (required, 1 only): \n{out}> """)
                        args.update({"crimeType": d_crime_type[int(crime_type)]})

                    # insert outcome
                    outcome = input("Enter the outcome of the entry: (optional)\n> ")
                    args.update({"outcome": outcome.strip()})

                    # insert context
                    context = input("Enter the context of the entry: (optional)\n> ")
                    args.update({"context": context.strip()})

                    # execution
                    if len(location_args) != 0 : make_insertion("Location", location_args, mydb)
                    make_insertion(joint_table, args, mydb)

                # Stop-and-Search
                elif table == '2':
                    
                    args = {}
                    joint_table = "StopAndSearch"

                    search_type = ''
                    while search_type == '': search_type = input("""Select type of searching: 
    \t1. Person search
        2. Vehicle search
        3. Person and vehicle search\n> """)
                    d_search_type = { 1 : 'Person search', 2 : 'Vehicle search', 3 : 'Person and Vehicle search'}
                    args.update({"type": d_search_type[int(search_type)]})


                    date = ""
                    date = input("Enter the date and time of the occurrence in the following format: yyyy-mm-dd hh:mm \n> ")
                    if date.strip() != '':
                        args.update({'date':date.strip()})

                    po = input("Was this stop-and-search part of a policing operation (Y/N):\n> ")
                    if po.upper() == "Y": 
                        args.update({'partOfPolicingOperation':1}) # 1 == True
                        args.update({'policingOperation':1})
                    elif po.upper() == "N":
                        args.update({'partOfPolicingOperation':0})
                        args.update({'policingOperation':0})

                    # legislation
                    tempList = ['Police and Criminal Evidence Act 1984 (section 1)', 'Misuse of Drugs Act 1971 (section 23)', 'Firearms Act 1968 (section 47)', 'Criminal Justice Act 1988 (section 139B)', 'Criminal Justice and Public Order Act 1994 (section 60)']
                    out = ""
                    d_legist = {}
                    for i, ele in enumerate(tempList):
                        out += f"\t{i+1}. {ele}\n"
                        d_legist[i+1] = ele
                    legist = input(f"""Select the object of search in the stop-and-search: (optional, 1 only)\n{out}> """)
                    entry = "" if legist == "" else d_legist[int(legist)]
                    args.update({"legislation": entry})
                    
                    tempList = ['Stolen goods', 'Controlled drugs', 'Article for use in theft', 'Offensive weapons', 'Articles for use in criminal damage', 'Fireworks', 'Evidence of offences under the Act', 'Anything to threaten or harm anyone', 'Firearms']
                    out = ""
                    d_object = {}
                    for i, ele in enumerate(tempList):
                        out += f"\t{i+1}. {ele}\n"
                        d_object[i+1] = ele
                    object = input(f"""Select the object of search in the stop-and-search: (optional, 1 only)\n{out}> """)
                    entry = "" if object == "" else d_object[int(object)]
                    args.update({"objectOfSearch": entry})

                    tempList = ['Suspect arrested', 'Nothing found - no further action', 'Offender given drugs possession warning', 'Local resolution', 'Offender given penalty notice', 'Suspect summonsed to court', 'Article found - Detailed outcome unavailable', 'Offender cautioned']
                    out = ""
                    d_outcome = {}
                    for i, ele in enumerate(tempList):
                        out += f"\t{i+1}. {ele}\n"
                        d_outcome[i+1] = ele
                    outcome = input(f"""Select the outcome in the stop-and-search: (optional, 1 only)\n{out}> """)
                    entry = "" if outcome == "" else d_outcome[int(outcome)]
                    args.update({"outcome":entry})


                    # person info
                    victim_args = {}
                    victim = input("Enter the person description of stop-and-search (Y/N or leave empty for all):\n> ")
                    if victim.strip() != "" and victim.strip().upper() != "N" :

                        age = input(
                        """Enter the Person information in the stop-and-search:
                        Select the age range (optional): 
    \t1. 10-17
        2. 18-24
        3. 25-34
        4. over 34\n> """)
                        d_age = {1: "10-17", 2: "18-24", 3: "25-34", 4: "over 34"}
                        entry = "" if age == "" else d_age[int(age)]
                        victim_args.update({"ageRange": entry})
                        
                        gender = input(
                        """Enter the gender of the subject (optional): 
    \t1: Male
        2: Female
        3: Other\n> """)
                        d_gender = {
        1: "Male",
        2: "Female",
        3: "Other"
                        }
                        entry = "" if gender == "" else d_gender[int(gender)]
                        victim_args.update({"gender": entry})
                        

                        race = input("""Select the ethnicity of the subject (optional): 
    \t1: Asian
        2: White
        3: Black
        4: Mixed
        5: Other\n> """)
                        d_race = {
        1: "Asian",
        2: "White",
        3: "Black",
        4: "Mixed",
        5: "Other"
                        }
                        entry = "" if race == "" else d_race[int(race)]
                        victim_args.update({"policeIdentifiedEthnicity": entry})

                    print(args)
                    sasID = make_insertion(joint_table, args, mydb)

                    if victim_args:
                        victim_args.update({ 'sasID' : sasID })
                        make_insertion("StopAndSearchPerson", victim_args, mydb)

                # victim stuff
                elif table == '3':

                    victim_args = {}

                    type = input("Is the victim involved in a crime (Y/N):\n> ")
                    joint_table = ""
                    if type.upper() == "Y": 
                        joint_table = "CrimeVictim"
                        crime_id = input("Enter the crime ID they were involved in:\n> ")
                        victim_args.update({'crimeID' : crime_id.strip()})

                    elif type.upper() == "N": 
                        joint_table = "NonCrimeVictim"

                        crime_id = input("Enter the non crime ID they were involved in:\n> ")
                        victim_args.update({'id' : crime_id.strip()})

                    else: continue

                    victim_age = input("Enter the victim age (optional):\n> ")
                    victim_args.update({"age": victim_age})

                    victim_gender = input(
                    """Enter the gender of the victim (optional): 
    \t1: Male
        2: Female
        3: Other\n> """)
                    d_gender = {
        1: "Male",
        2: "Female",
        3: "Other"
                    }
                    entry = "" if victim_gender == "" else d_gender[int(victim_gender)]
                    victim_args.update({"gender": entry})

                    victim_ethnicity = input("""Select the ethnicity of the victim (optional): 
    \t1: Asian
        2: White
        3: Black
        4: Mixed
        5: Other\n> """)
                    d_race = {
        1: "Asian",
        2: "White",
        3: "Black",
        4: "Mixed",
        5: "Other"
                    }
                    entry = "" if victim_ethnicity == "" else d_race[int(victim_ethnicity)]
                    victim_args.update({"ethnicity": entry})
                    
                    make_insertion(joint_table, victim_args, mydb)

                # loop insert
                else: continue

            # QUERY data
            elif user_option == '2':
                
                table = input(
                    """Select the scope of data you wish to query on (enter nothing to return to the previous menu):
    \t1. Crime numbers by LSOA Code
        2. Crimes and Non-crimes
        3. Stop-and-Search\n> """)
                
                # Crime by LSOA Code
                if table == '1':
                    query_CrimeCount(cursor)
                    
                
                # Crimes and Non-crimes
                elif table == '2':
                    query_crime_non(cursor)
                    

                # Stop-and-Search
                elif table == '3':
                    query_StopAndSearch(cursor)

                # loop
                else: continue
                
            # MODIFY data
            elif user_option == '3':

                table = input(
                    """Select the table that you wish to update (enter nothing to return to the previous menu):
    \t1. Crimes and Non-crimes
        2. Stop-and-Search
        3. Location\n> """)

                # Crimes and Non-crimes
                if table == '1':

                    # outcomes and streets
                    # if anti social, no crime id
                    args = {}
                    where_args = {}

                    behaviour = input("""Which table are you updating in: 
    \t1. Crime
        2. Non-crime\n>  """).strip()
                    
                    if behaviour.upper() == '1': joint_table = "Crime"
                    elif behaviour.upper() == '2': joint_table = "NonCrime"
                    else: continue

                    if joint_table == "Crime" :
                        crime_id = ''
                        while crime_id == '': crime_id = input("Enter the crime id (required):\n> ")
                        where_args.update({"crimeID": crime_id})
                    else:
                        crime_id = ''
                        while crime_id == '': crime_id = input("Enter the id (required):\n> ")
                        where_args.update({"id": crime_id})

                    date = ''
                    date = input("What is the new date of crime in the following format yyyy-mm-dd hh:mm (leave empty if unchanged):\n> ")
                    date = date.strip()
                    if date != '':
                        args.update({"date": date})
                    
                    falls_within = input("""New reporter of data?: 
    \t1. Metropolitan Police Service 
        2. City of London Police\n> """)
                    d_falls = {1: "Metropolitan Police Service", 2: "City of London Police"}
                    if falls_within.strip() != '':
                        args.update({"fallsWithin": d_falls[int(falls_within)]})


                    reported_by = input("""New provider of data?: 
    \t1. Metropolitan Police Service 
        2. City of London Police\n> """)
                    if reported_by.strip() != '':
                        args.update({"reportedBy": d_falls[int(reported_by)]})

                    # update location separately

                    # only if crime table
                    if joint_table == "Crime":
                        tempList = ['Vehicle crime', 'Violence and sexual offences', 'Bicycle theft', 'Other theft', 'Theft from the person', 'Other crime', 'Drugs', 'Burglary', 'Public order', 'Shoplifting', 'Criminal damage and arson', 'Robbery', 'Possession of weapons']
                        out = ""
                        d_crime_type = {}
                        for i, ele in enumerate(tempList):
                            out += f"\t{i+1}. {ele}\n"
                            d_crime_type.update({i+1: ele})
                        crime_type = input(f"""Select the type of crime (leave empty if all): \n{out}> """)
                        
                        if crime_type.strip() != '':
                            args.update({"crimeType": d_crime_type[int(crime_type)]})

                    # update outcome
                    outcome = input("Update the outcome of the entry:\n> ")
                    if outcome.strip() != '':
                        args.update({"outcome": outcome.strip()})

                    # update context
                    context = input("Update the context of the entry:\n> ")
                    if context.strip() != '':
                        args.update({"context": context.strip()})

                    # execution
                    make_update(joint_table, args, where_args, mydb)

                # Stop-and-Search
                elif table == '2':
                    
                    args = {}
                    where_args = {}
                    joint_table = "StopAndSearch"

                    sasID = ''
                    while sasID == '': sasID = input("Enter the stop-and-search ID (required):\n> ")
                    where_args.update({"sasID": sasID})

                    search_type = ''
                    search_type = input("""Update type of search (leave empty if unchanged):
    \t1. Person search
        2. Vehicle search
        3. Person and vehicle search\n> """)
                    d_search_type = { 1 : 'Person search', 2 : 'Vehicle search', 3 : 'Person and Vehicle search'}
                    if search_type.strip() != '':
                        args.update({ 'type' : d_search_type[int(search_type)] })


                    po = input("Was the stop-and-search part of a policing operation (Y/N):\n>  ")
                    if po.upper() == "Y": 
                        args.update({'partOfPolicingOperation':1})
                        args.update({'policingOperation':1})
                    elif po.upper() == "N":
                        args.update({'partOfPolicingOperation':0})
                        args.update({'policingOperation':0})

                    # legislation
                    tempList = ['Police and Criminal Evidence Act 1984 (section 1)', 'Misuse of Drugs Act 1971 (section 23)', 'Firearms Act 1968 (section 47)', 'Criminal Justice Act 1988 (section 139B)', 'Criminal Justice and Public Order Act 1994 (section 60)']
                    out = ""
                    d_legist = {}
                    for i, ele in enumerate(tempList):
                        out += f"\t{i+1}. {ele}\n"
                        d_legist[i+1] = ele
                    legist = input(f"""Select the object of search in the stop-and-search: (optional, 1 only)\n{out}> """)
                    entry = "" if legist == "" else d_legist[int(legist)]
                    if legist.strip() != '':
                        args.update({"legislation": entry})

                    tempList = ['Stolen goods', 'Controlled drugs', 'Article for use in theft', 'Offensive weapons', 'Articles for use in criminal damage', 'Fireworks', 'Evidence of offences under the Act', 'Anything to threaten or harm anyone', 'Firearms']
                    out = ""
                    d_object = {}
                    for i, ele in enumerate(tempList):
                        out += f"\t{i+1}. {ele}\n"
                        d_object[i+1] = ele
                    object = input(f"""Update object of search in the stop-and-search (leave empty if unchanged): \n{out}> """)
                    if object.strip() != '':
                        args.update({'objectOfSearch' : d_object[int(object)]})

                    tempList = ['Suspect arrested', 'Nothing found - no further action', 'Offender given drugs possession warning', 'Local resolution', 'Offender given penalty notice', 'Suspect summonsed to court', 'Article found - Detailed outcome unavailable', 'Offender cautioned']
                    out = ""
                    d_outcome = {}
                    for i, ele in enumerate(tempList):
                        out += f"\t{i+1}. {ele}\n"
                        d_outcome[i+1] = ele
                    outcome = input(f"""Update the outcome in the stop-and-search (leave empty if unchanged): \n{out}> """)
                    if outcome.strip() != '':
                        args.update({'outcome' : d_outcome[int(outcome)]})

                    make_update(joint_table, args, where_args, mydb)

                # Location
                elif table == '3':
                    # update location

                    where_args = {}
                    location_args = {}

                    long, lat = "", ""
                    while long == "": long = input("Enter the longitude (required):\n> ")
                    while lat == "": lat = input("Enter the latitude (required):\n> ")
                    locDesc = input("Enter the location description (preferrably a street name) (optional):\n> ")
                    if locDesc.strip() != '':
                        location_args.update({'locationDescription':locDesc})
                    LSOAcode = input("Enter the LSOA code associated with the location (optional):\n> ")
                    if LSOAcode.strip() != '':
                        location_args.update({'LSOAcode':LSOAcode})

                    where_args.update({ 
                        'longitude':int(float(long) * 1000000), 
                        'latitude':int(float(lat) * 1000000)
                    })

                    make_update("Location", location_args, where_args, mydb)

                # loop
                else: continue

            # loop
            else: continue
        
        except Exception as e:
            print(f"Error > {e}")
        
if __name__ == "__main__":
    main()
